## -*- coding: utf-8 -*-
import requests
import re


def edit_comment(cur_comment):
    cur_comment = re.sub("http\S*", "", cur_comment) #deleting links
    cur_comment = re.sub("\n", " ", cur_comment)
    return re.sub("[[].*?,", "", cur_comment) #deleting appeals


def get_age(res):
    if res[0].get('bdate') is None:
        return 0
    date = res[0]['bdate']
    cnt_dots = 0
    str_age = ""
    for symbol in date:
        if symbol == '.':
            cnt_dots += 1
            str_age = ""
        else:
            str_age += symbol

    if cnt_dots < 2:
        return 0
    return 2016 - int(str_age)


def user_info(id, user_comments):
    USER_GET = 'http://api.vk.com/method/users.get'
    res = requests.get(USER_GET, {
        'user_ids': id,
        'fields': 'bdate',
        'v': 5.26
    }).json()['response']
    data = {}

    if (get_age(res) != 0):
        age = get_age(res)
        if age <= 18:
            data['class'] = 0
        elif age <= 25:
            data['class'] = 1
        elif age <= 35:
            data['class'] = 2
        elif age <= 45:
            data['class'] = 3
        else:
            data['class'] = 4
        data['comments'] = user_comments
    else:
        data['class'] = -1

    return data


def get_comments(group_id, number_of_posts):
    all_data = []
    WALL_GET = 'http://api.vk.com/method/wall.get'
    WALL_GET_COMMENTS = 'http://api.vk.com/method/wall.getComments'
    #getting posts from the wall
    res = requests.get(WALL_GET, {
        'owner_id': -group_id,
        'count': number_of_posts,
        'v' : 5.26
    }).json()['response']

    for post in res['items']:
        comments = requests.get(WALL_GET_COMMENTS, {
            'owner_id': -group_id,
            'post_id': post['id'],
            'v' : 5.26
        }).json()['response']
        #adding comments to a fixed person
        for cur_comment in comments['items']:
            if cur_comment['from_id'] < 0:
                continue
            comment_edition = edit_comment(cur_comment['text'])
            user_data = user_info(cur_comment['from_id'], comment_edition)
            if user_data['class'] != -1:
                all_data.append(user_data)

    return all_data


def is_bad_group(group_id):
    WALL_GET = 'http://api.vk.com/method/wall.get'
    res = requests.get(WALL_GET, {
        'owner_id': -group_id,
        'v' : 5.26
    }).json()
    if res.get('response') is None:
        return 1
    return 0


def get_posts_count(group_id):
    WALL_GET = 'http://api.vk.com/method/wall.get'
    res = requests.get(WALL_GET, {
        'owner_id': -group_id,
        'v' : 5.26
    }).json()['response']
    return res['count']


#29534144
#93867488