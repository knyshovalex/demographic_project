import numpy
import re


from nltk.corpus import stopwords
from scipy import sparse
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler


def cnt_punct(comment):
    #counting punctuation
    mark = '.!?,; )(":-'
    res = 0
    for each_symbol in comment:
        if mark.find(each_symbol) != -1:
            res += 1
    return res


def longest_word(comment):
    #finding the longest word in the comment
    mark = '.!?,; )(":-'
    res = cur = 0
    for each_symbol in comment:
        if mark.find(each_symbol) != -1:
            cur = 0
        else:
            cur += 1
            res = max(res, cur)
    return res


class Classifier:
    def __init__(self):
        self.address = re.compile('^\w+\s*,\s*')
        self.html = re.compile(r"</?\w+[^>]*>")
        self.vk_tags = re.compile(r'(\s|^)#\w+')
        self.nums = re.compile(r'\d')
        self.stop_words = stopwords.words('russian')
        self.english_words = re.compile(r'([a-zA-Z]*)')


        # model estimators
        self.vectorizer = TfidfVectorizer(max_df=0.8, ngram_range=(1, 5), stop_words=self.stop_words)
        self.classifier = LogisticRegression()
        self.scaler = MinMaxScaler()

    def extract(self, samples):
        #custom features
        features = []
        for cur_comment in samples:
            cur_features = []
            cur_features.append(cnt_punct(cur_comment) / len(cur_comment))
            cur_features.append(longest_word(cur_comment) / len(cur_comment))
            features.append(cur_features)
        return numpy.array(features)

    def fit(self, samples, labels):
        texts = [self.clean_text(i) for i in samples]
        # extracting custom features
        custom_features = self.extract(samples)
        custom_features = sparse.csr_matrix(self.scaler.fit_transform(custom_features))
        # extracting n_grams
        n_gram_features = self.vectorizer.fit_transform(texts)
        features = sparse.hstack([n_gram_features, custom_features], format='csr')
        #print(self.vectorizer.get_feature_names())
        self.classifier.fit(features, labels)

    def predict(self, samples):
        texts = [self.clean_text(i) for i in samples]
        # extracting n_grams
        n_gram_features = self.vectorizer.transform(texts)
        # extracting custom features
        custom_features = self.extract(samples)
        custom_features = sparse.csr_matrix(self.scaler.fit_transform(custom_features))
        features = sparse.hstack([n_gram_features, custom_features], format='csr')
        return self.classifier.predict(features)

    def clean_text(self, text):
        #text cleaning
        text = self.address.sub('', text)
        text = self.html.sub('', text)
        text = self.nums.sub('', text)
        text = text.replace('_', ' ')
        text = text.replace('...', '.')
        text = self.vk_tags.sub('', text)
        text = self.english_words.sub('', text)
        return text

    def __str__(self):
        return str(self.__dict__)
