import homework
import io
import random
import re

from homework import get_comments, get_posts_count, is_bad_group

comments_out = io.open('comments_train.txt','w', encoding='utf8')
classes_out = io.open('classes_train.txt','w', encoding='utf8')
comments_train = io.open('comments_test.txt','w', encoding='utf8')
classes_train = io.open('classes_test.txt','w', encoding='utf8')

groups = [73048120, 29534144, 15755094, 80358348, 8722610, 37008294, 28658784, 93867488, 77270571, 63731512, 32665698,
          26493942, 18496184, 41781422, 68988957, 30315369, 57354358, 37725105, 93867488, 66036248, 43215063]
for cur_group in groups:
    cur_data = get_comments(cur_group, 100)
    random.shuffle(cur_data)
    for each_data in cur_data:
        if re.search('[а-я]', each_data['comments']) is None:
            continue
        if random.randint(0, 4) == 0:
            print(each_data['comments'], file = comments_train)
            print(each_data['class'], file = classes_train)
        else:
            print(each_data['comments'], file = comments_out)
            print(each_data['class'], file = classes_out)
    print(cur_group)

for cur_group in range(1, 100000):#groups:
    if is_bad_group(cur_group):
        continue

    cur_data = get_comments(cur_group, min(10, get_posts_count(cur_group)))
    random.shuffle(cur_data)
    for each_data in cur_data:
        if re.search('[а-я]', each_data['comments']) is None:
            continue
        if random.randint(0, 4) == 0:
            print(each_data['comments'], file = comments_train)
            print(each_data['class'], file = classes_train)
        else:
            print(each_data['comments'], file = comments_out)
            print(each_data['class'], file = classes_out)


