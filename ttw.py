import nltk
import Stemmer

s = str(input())
ans = ""
i = 0
mark = {}
mark['.'] = mark['!'] = mark['?'] = 1
mark[','] = mark[';'] = 1
mark[' '] = 1
mark[')'] = mark['('] = mark['['] = mark[']'] = mark['{'] = mark['}'] = mark['"'] = 1
mark[':'] = 1
mark['-'] = 1


from nltk.corpus import stopwords
nltk.download("stopwords")
stop = stopwords.words('russian')
#print(stop)
stemmer = Stemmer.Stemmer('russian')
print(stemmer.stemWord('ковриков'))

for c in s:
    if (c == '-' and s[i - 1] != ' ' and s[i + 1] != ' '):
        i += 1
        ans += c
        continue
    elif(mark.get(c) == 1):
        if ans != "" and ans not in stop:
            print(stemmer.stemWord(ans), " ")
        ans = ""
    else:
        ans += c
    i+= 1

if (ans != "" and ans not in stop):
    print(ans, ' ')